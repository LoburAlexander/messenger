Application for exchanging messages between users, based on web sockets.<br>
Uses custom JSON-based message exchanging protocol.<br>
Provides protected connection between client and server.<br>

Structure:
<ul>
<li>Web server</li>
<li>MySQL database</li>
<li>Android application</li>
</ul>